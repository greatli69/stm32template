/*
 * hal_i2c.c
 *
 *  Created on: 2015�~6��15��
 *      Author: Thomas
 */
#include "hal_i2c.h"
#include "hal_gpio.h"

DMA_InitTypeDef  sEEDMA_InitStructure;

static void I2C_GpioInit(void *sck,void *sda)
{
  HAL_GPIO_Init((const GPIOx_TypeDef *)sck,GPIO_Mode_AF_OD);
  HAL_GPIO_Init((const GPIOx_TypeDef *)sda,GPIO_Mode_AF_OD);
}

void HAL_I2C_Init(I2C_TypeDef *I2Cx)
{
	I2C_InitTypeDef I2C_InitStructure;

	if(I2Cx == I2C1){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, DISABLE);
		I2C_GpioInit((void *)PB6,(void *)PB7);
	}else if(I2Cx == I2C2){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, DISABLE);
		I2C_GpioInit((void *)PB10,(void *)PB11);
	}

	I2C_DeInit(I2Cx);
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = I2C_MASTER_ADDR;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = I2C_CLOCKSPEED;
	I2C_Init(I2Cx, &I2C_InitStructure);


	HAL_NVIC_Config(I2C1_EV_IRQn,NVIC_PriorityGroup_0,0);
	I2C_ITConfig(I2Cx,I2C_IT_EVT|I2C_IT_BUF|I2C_IT_ERR,ENABLE);

	I2C_Cmd(I2Cx, ENABLE);

#if 0
	HAL_NVIC_Config(DMA1_Channel6_IRQn,NVIC_PriorityGroup_0,0);
	HAL_NVIC_Config(DMA1_Channel7_IRQn,NVIC_PriorityGroup_0,0);

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	/* I2C TX DMA Channel configuration */
	DMA_DeInit(DMA1_Channel6);
	sEEDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&I2C1->DR;
	sEEDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)0;   /* This parameter will be configured durig communication */
	sEEDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;    /* This parameter will be configured durig communication */
	sEEDMA_InitStructure.DMA_BufferSize = 0xFFFF;            /* This parameter will be configured durig communication */
	sEEDMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	sEEDMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	sEEDMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
	sEEDMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	sEEDMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	sEEDMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	sEEDMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel6, &sEEDMA_InitStructure);

	/* I2C RX DMA Channel configuration */
	DMA_DeInit(DMA1_Channel7);
	DMA_Init(DMA1_Channel7, &sEEDMA_InitStructure);

	/* Enable the DMA Channels Interrupts */
	DMA_ITConfig(DMA1_Channel6, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE);
#endif
}

void HAL_I2C_EEP_Write(I2C_TypeDef *I2Cx,uint8_t *pDataBuffer,uint8_t AddrOfWrite)
{
	I2C_GenerateSTART(I2Cx,ENABLE);
	while(!I2C_CheckEvent(I2Cx,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	/* Send EEPROM address for write */
	I2C_Send7bitAddress(I2Cx,EEPROM_ADDRESS,I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	I2C_SendData(I2Cx,AddrOfWrite);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	I2C_GenerateSTOP(I2C1, ENABLE);
}
