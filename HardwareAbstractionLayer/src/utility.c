/*
 * utility.c
 *
 *  Created on: 2015年6月8日
 *      Author: Thomas
 */
#include "utility.h"
#include "hal_timer.h"
#include <string.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t ChipUniqueID[3];
/* Private function prototypes -----------------------------------------------*/
static void movblkh (const void *sur, void *des, int16_t len);
static void movblk (const void *sur, void *des, int16_t len);

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

int8_t switchLettersUppercase(unsigned char *str)
{
	unsigned char i;
	for (i = 0; str[i] != 0; i++)
	{
		if (!isprint(str[i]) || str[i] == ',')
			return -1;
		str[i] = toupper(str[i]);
	}
	return 0;
}

uint8_t getChipID(uint32_t (*chipUniqueID)[3])
{
	(*chipUniqueID)[2] = *(__IO uint32_t *)(0X1FFFF7E8);
	(*chipUniqueID)[1] = *(__IO uint32_t *)(0X1FFFF7EC);
	(*chipUniqueID)[0] = *(__IO uint32_t *)(0X1FFFF7F0);

	if((*chipUniqueID)[0] == 0 || (*chipUniqueID)[1] == 0 || (*chipUniqueID)[2] == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(uint32_t nTime)
{
	uint32_t t;
	t = systick;
	while((uint32_t)(systick - t) < nTime);
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
#ifdef OPEN_DEBUG_MESSAGE
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
  ochar(DEBUG_PORT,(uint8_t) ch);
#endif
  return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @brief  Convert a string to an integer
  * @param  inputstr: The string to be converted
  * @param  intnum: The intger value
  * @retval 1: Correct
  *         0: Error
  */
uint32_t Str2Int(uint8_t *inputstr, int32_t *intnum)
{
  uint32_t i = 0, res = 0;
  uint32_t val = 0;

  if (inputstr[0] == '0' && (inputstr[1] == 'x' || inputstr[1] == 'X'))
  {
    if (inputstr[2] == '\0')
    {
      return 0;
    }
    for (i = 2; i < 11; i++)
    {
      if (inputstr[i] == '\0')
      {
        *intnum = val;
        /* return 1; */
        res = 1;
        break;
      }
      if (ISVALIDHEX(inputstr[i]))
      {
        val = (val << 4) + CONVERTHEX(inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
    }
    /* over 8 digit hex --invalid */
    if (i >= 11)
    {
      res = 0;
    }
  }
  else /* max 10-digit decimal input */
  {
    for (i = 0;i < 11;i++)
    {
      if (inputstr[i] == '\0')
      {
        *intnum = val;
        /* return 1 */
        res = 1;
        break;
      }
      else if ((inputstr[i] == 'k' || inputstr[i] == 'K') && (i > 0))
      {
        val = val << 10;
        *intnum = val;
        res = 1;
        break;
      }
      else if ((inputstr[i] == 'm' || inputstr[i] == 'M') && (i > 0))
      {
        val = val << 20;
        *intnum = val;
        res = 1;
        break;
      }
      else if (ISVALIDDEC(inputstr[i]))
      {
        val = val * 10 + CONVERTDEC(inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
    }
    /* Over 10 digit decimal --invalid */
    if (i >= 11)
    {
      res = 0;
    }
  }

  return res;
}

void pattern(void *block, uint8_t c, uint16_t size)
{
	for (; size > 0; size--, VOID_CAST(block)++)
		*(uint8_t *) block= c;
}

void imovblk (const void *sur, void *des, int16_t len)
{
	if ((uint8_t *) sur >= (uint8_t *) des)
		movblkh(sur, des, len);
	else
		movblk(sur, des, len);
}

static void movblkh (const void *sur, void *des, int16_t len)
{
	for (; len > 0; len--, VOID_CAST(des)++, VOID_CAST(sur)++)
		*(uint8_t *) des = *(uint8_t *) sur;
}

static void movblk (const void *sur, void *des, int16_t len)
{
	len--;
	for (VOID_CAST(sur) += len, VOID_CAST(des) += len; len >= 0;
			len--, VOID_CAST(des)--, VOID_CAST(sur)--)
		*(uint8_t *) des = *(uint8_t *) sur;
}

int8_t cmpblk(const void *sur, const void *des, int8_t len)
{
	len--;
	for (VOID_CAST(sur) += len, VOID_CAST(des) += len; len >= 0;
			len--, VOID_CAST(sur)--, VOID_CAST(des)--)
	{
		if (*(uint8_t *) sur > *(uint8_t *) des)
			return 1;
		else if (*(uint8_t *) sur < *(uint8_t *) des)
			return -1;
	}
	return 0;
}

int8_t convertDecToAscii(uint8_t c)
{
 if(c < 10) return c + '0';
 else return c - 10 + 'A';
}

/**
  * @brief  Convert an Integer to a string
  * @param  str: The string
  * @param  intnum: The intger to be converted
  * @retval None
  */
void Int2Str(uint8_t* str, int32_t intnum)
{
  uint32_t i, Div = 1000000000, j = 0, Status = 0;

  for (i = 0; i < 10; i++)
  {
    str[j++] = (intnum / Div) + 48;

    intnum = intnum % Div;
    Div /= 10;
    if ((str[j-1] == '0') & (Status == 0))
    {
      j = 0;
    }
    else
    {
      Status++;
    }
  }
}

uint32_t strLen(const char *pStr)
{
	uint32_t count;

	for(count = 0; *pStr != '\0'; count++, pStr++);

	return count;
}

void strCat(char* str1, const char* str2)
{
	uint32_t i, j = strLen(str1);

	for (i = 0; i < strLen(str2); i++)
	{
		str1[j] = str2[i];
		j++;
	}
}

uint8_t * findChar(uint8_t pStr[],const uint8_t c , const uint8_t pos)
{
	uint8_t i;
	uint8_t times;

	if(!pos)
	{
		return NULL;
	}

	for(i = 0, times = 0; pStr[i] != 0; i++)
	{
		if(pStr[i] == c)
		{
			times++;
			if(pos == times)
			{
				return &pStr[i];
			}
		}
	}
	return NULL;
}

/* CRC16 implementation acording to CCITT standards */
const uint16_t crc16tab[256]=
{
0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

uint16_t crc16_ccitt(const uint8_t *buf, int32_t len)
{
	register uint32_t counter;
	register uint16_t crc = 0;
	for (counter = 0; counter < len; counter++)
		crc = (crc << 8) ^ crc16tab[((crc >> 8) ^ *(uint8_t *) buf++) & 0x00FF];
	return crc;
}

/**
  * @brief  校驗
  * @param	buf:校驗的原始數據
  * @param	crcAddr:欲校驗的CRC位址
  * @retval 校驗正確返回1，校驗錯誤則為0
  */
uint32_t checkCRCResult(const uint8_t *buf, uint32_t crcAddr)
{
	uint16_t crc = crc16_ccitt(buf, crcAddr);
	uint16_t tcrc = (buf[crcAddr] << 8) + buf[crcAddr + 1];
	if (crc == tcrc)
		return 1;
	return 0;
}
