/*
 * hal_rcc.c
 *
 *  Created on: 2015�~6��9��
 *      Author: Thomas
 */
#include "hal_rcc.h"

uint8_t rccFlag[1];

void HAL_RCC_CSSENABLE(void)
{
	/* Enable Clock Security System(CSS): this will generate an NMI exception
		when HSE clock fails */
	RCC_ClockSecuritySystemCmd(ENABLE);

	/* Enable and configure RCC global IRQ channel */
	HAL_NVIC_Config(RCC_IRQn,NVIC_PriorityGroup_0,0);
}


/**
  * @brief  when power on, use this func to get reset from which source and record on flag
  * @param  None
  * @retval None
  */
void getResetSourceFlag(void)
{
	if(RCC_GetFlagStatus(RCC_FLAG_PINRST) !=RESET)
	{
		SET_RESET_SOURCE_NRST;
	}
	else
	{
		CLR_RESET_SOURCE_NRST;
	}

	if(RCC_GetFlagStatus(RCC_FLAG_SFTRST) !=RESET)
	{
		SET_RESET_SOURCE_SOFTWARE;
	}
	else
	{
		CLR_RESET_SOURCE_SOFTWARE;
	}

	if(RCC_GetFlagStatus(RCC_FLAG_PORRST) !=RESET)
	{
		SET_RESET_SOURCE_POR;
	}
	else
	{
		CLR_RESET_SOURCE_POR;
	}

	if(IS_RESET_SOURCE_NRST)
	{
		if(IS_RESET_SOURCE_SOFTWARE || IS_RESET_SOURCE_POR)
		{
			RCC_ClearFlag();
			CLR_RESET_SOURCE_NRST;
			CLR_RESET_SOURCE_SOFTWARE;
			CLR_RESET_SOURCE_POR;
		}
	}

}
