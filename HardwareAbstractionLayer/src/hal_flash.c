/*
 * hal_flash.c
 *
 *  Created on: 2015�~6��12��
 *      Author: Thomas
 */
#include "hal_flash.h"
#include "stm32f10x_flash.h"

uint32_t NbrOfPage = 0x00;
uint32_t WRPR_Value = 0xFFFFFFFF, ProtectedPages = 0x0;

int8_t HAL_FLASH_EarsePage(uint32_t startAdrr, uint32_t endAdrr)
{
	uint32_t EraseCounter = 0x00;
	FLASH_Status FLASHStatus = FLASH_COMPLETE;

	if(((endAdrr - startAdrr)%FLASH_PAGE_SIZE))	return -1;

	FLASH_Unlock();

	/* Define the number of page to be erased */
	NbrOfPage = (endAdrr - startAdrr) / FLASH_PAGE_SIZE;

	/* Clear All pending flags */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);

	/* Erase the FLASH pages */
	for(EraseCounter = 0; (EraseCounter < NbrOfPage) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++)
	{
	  FLASHStatus = FLASH_ErasePage(startAdrr + (FLASH_PAGE_SIZE * EraseCounter));
	}
	return 0;
}

uint16_t HAL_FLASH_GetHalfWordData(uint32_t startAdrr)
{
	return *(__IO uint16_t*) startAdrr;
}

int8_t HAL_FLASH_ProgramHalfWordData(uint32_t startAdrr,uint16_t Data)
{
	uint32_t Address = 0x00;
	FLASH_Status FLASHStatus = FLASH_COMPLETE;

	if((startAdrr%2) != 0)	return -1;
	/* Program Flash Bank1 */
	Address = startAdrr;

	FLASHStatus = FLASH_ProgramHalfWord(Address, Data);
	if(FLASHStatus != FLASH_COMPLETE)	return -1;

	/* Check the correctness of written data */
	if (HAL_FLASH_GetHalfWordData(Address) != Data)	return -1;

	return 0;
}

int8_t HAL_FLASH_CancelWriteProtectionPage(uint32_t FLASH_WRProt_Pages)
{
	FLASH_Status FLASHStatus = FLASH_COMPLETE;
	FLASH_Unlock();
	/* Get pages write protection status */
	WRPR_Value = FLASH_GetWriteProtectionOptionByte();

	/* Get pages already write protected */
	ProtectedPages = ~(WRPR_Value | FLASH_WRProt_Pages);

	/* Check if desired pages are already write protected */
	if((WRPR_Value | (~FLASH_WRProt_Pages)) != 0xFFFFFFFF )
	{
		/* Erase all the option Bytes */
		FLASHStatus = FLASH_EraseOptionBytes();
		if(FLASHStatus != FLASH_COMPLETE)	return -1;

		/* Check if there is write protected pages */
		if(ProtectedPages != 0x0)
		{
		  /* Restore write protected pages */
		  FLASHStatus = FLASH_EnableWriteProtection(ProtectedPages);
		  if(FLASHStatus != FLASH_COMPLETE)	return -1;
		}
		/* Generate System Reset to load the new option byte values */
		NVIC_SystemReset();
	}
	return 0;
}

int8_t HAL_FLASH_SetWriteProtectionPage(uint32_t FLASH_WRProt_Pages)
{
	FLASH_Status FLASHStatus = FLASH_COMPLETE;
	FLASH_Unlock();
	/* Get pages write protection status */
	WRPR_Value = FLASH_GetWriteProtectionOptionByte();

	/* Get current write protected pages and the new pages to be protected */
	ProtectedPages =  (~WRPR_Value) | FLASH_WRProt_Pages;

	/* Check if desired pages are not yet write protected */
	if(((~WRPR_Value) & FLASH_WRProt_Pages )!= FLASH_WRProt_Pages)
	{
		/* Erase all the option Bytes because if a program operation is
		  performed on a protected page, the Flash memory returns a
		  protection error */
		FLASHStatus = FLASH_EraseOptionBytes();
		if(FLASHStatus != FLASH_COMPLETE)	return -1;

		/* Enable the pages write protection */
		FLASHStatus = FLASH_EnableWriteProtection(ProtectedPages);
		if(FLASHStatus != FLASH_COMPLETE)	return -1;
		/* Generate System Reset to load the new option byte values */
		NVIC_SystemReset();
	}
	return 0;
}

void HAL_FLASH_EarseOnePage(uint32_t startAdrr)
{
	HAL_FLASH_EarsePage(startAdrr, startAdrr + FLASH_PAGE_SIZE);
}
