/*
 * hal_usart.c
 *
 *  Created on: 2015年6月4日
 *      Author: Thomas
 */

#include "hal_usart.h"

static USART_InitTypeDef myUSARTConfig;
UART_TypeDef uart1,uart2,uart3;
static int8_t USARTPutToQueueData(UART_TypeDef *uartx,uint8_t c);
static int16_t USARTGetFromQueueData(UART_TypeDef *uartx);
static void USARTIrqRoutine(USART_TypeDef *USARTx, UART_TypeDef *uartx);
static int16_t USARTRecevieByte(USART_TypeDef *USARTx);
static void USARTSendByte(USART_TypeDef *USARTx,uint8_t c);
static void USARTPinInit(const GPIOx_TypeDef *Tx,const GPIOx_TypeDef *Rx);

/**
  * @brief  Print a string on the HyperTerminal
  * @param  s: The string to be printed
  * @retval None
  */
void Serial_PutString(USART_TypeDef *USARTx, uint8_t *s)
{
	while (*s != '\0')
	{
		ochar(USARTx, *s);
		s++;
	}
}

void uartCircularQueueReInit(UART_TypeDef *uartx)
{
	uartx->flag.tranRunning = 0;
	uartx->flag.recvData = 0;
	uartx->uartRx.front=0;
	uartx->uartRx.rear=0;
	uartx->uartTx.front=0;
	uartx->uartTx.rear=0;
	uartx->useINT = DISABLE;
}

void HAL_USART_USARTConfig(uint32_t baudrate,int8_t odevity,uint8_t length,int8_t stopbit)
{
	myUSARTConfig.USART_BaudRate = baudrate;
	if(stopbit == 1)
		myUSARTConfig.USART_StopBits=USART_StopBits_1;
	else if(stopbit == 2)
		myUSARTConfig.USART_StopBits=USART_StopBits_2;

	if(odevity == 'n' || odevity == 'N')
		myUSARTConfig.USART_Parity=USART_Parity_No;
	else if(odevity == 'o' || odevity == 'O')
		myUSARTConfig.USART_Parity=USART_Parity_Odd;
	else if(odevity == 'e' || odevity == 'E')
		myUSARTConfig.USART_Parity=USART_Parity_Even;

	if(length == 8)
		myUSARTConfig.USART_WordLength = USART_WordLength_8b;
	else if(length == 9)
		myUSARTConfig.USART_WordLength = USART_WordLength_9b;

	myUSARTConfig.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	myUSARTConfig.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
}

USART_InitTypeDef *getUSARTConfig(void)
{
	return &myUSARTConfig;
}

void HAL_USART_USART1_USE_CTS_RTS_Init(void)
{
	USART_InitTypeDef *tmpUSARTConfig;
	tmpUSARTConfig = getUSARTConfig();
	if (!IS_USART_WORD_LENGTH(tmpUSARTConfig->USART_WordLength) ||
	!IS_USART_STOPBITS(tmpUSARTConfig->USART_StopBits) ||
	!IS_USART_PARITY(tmpUSARTConfig->USART_Parity) ||
	!IS_USART_MODE(tmpUSARTConfig->USART_Mode) ||
	!IS_USART_HARDWARE_FLOW_CONTROL(tmpUSARTConfig->USART_HardwareFlowControl))
	{
		HAL_USART_USARTConfig(115200, 'N', 8, 1);
	}
	myUSARTConfig.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	USARTPinInit(PA9, PA10);	/* TX , Rx */
	USARTPinInit(PA12, PA11);	/* RTS, CTS */
	uartCircularQueueReInit(&uart1);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	HAL_NVIC_Config(USART1_IRQn, NVIC_PriorityGroup_0, 0);
	uart1.useINT = ENABLE;
	/* USART configuration */
	USART_Init(USART1, getUSARTConfig());
	/* Enable USART */
	USART_Cmd(USART1, ENABLE);

}

/**
 * @brief  Initiate USART
 * @param  USARTx:USART1、USART2、USART3...
 * @param  enableRemap:是否uart pin腳要映射
 * @param  useINT:是否使用中斷
 * @retval None
 */
void HAL_USART_Init(USART_TypeDef *USARTx,uint8_t enableRemap,FunctionalState useINT)
{
	USART_InitTypeDef *tmpUSARTConfig;
	tmpUSARTConfig = getUSARTConfig();

	if (!IS_USART_WORD_LENGTH(tmpUSARTConfig->USART_WordLength) ||
	!IS_USART_STOPBITS(tmpUSARTConfig->USART_StopBits) ||
	!IS_USART_PARITY(tmpUSARTConfig->USART_Parity) ||
	!IS_USART_MODE(tmpUSARTConfig->USART_Mode) ||
	!IS_USART_HARDWARE_FLOW_CONTROL(tmpUSARTConfig->USART_HardwareFlowControl))
	{
		HAL_USART_USARTConfig(115200, 'N', 8, 1);
	}

	if ((uint32_t) USARTx == (uint32_t) USART1)
	{
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		if (enableRemap == REMAP_ENABLE)
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
			GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
			USARTPinInit(PB6, PB7);
		}
		else
		{
			USARTPinInit(PA9, PA10);
		}
		uartCircularQueueReInit(&uart1);
	}
	else if ((uint32_t) USARTx == (uint32_t) USART2)
	{
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		if (enableRemap == REMAP_ENABLE)
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
			GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
			USARTPinInit(PD5, PD6);
		}
		else
		{
			USARTPinInit(PA2, PA3);
		}
		uartCircularQueueReInit(&uart2);
	}
	else if ((uint32_t) USARTx == (uint32_t) USART3)
	{
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
		if (enableRemap == REMAP_ENABLE)
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
			GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);
			USARTPinInit(PD8, PD9);
		}
		else
		{
			USARTPinInit(PB10, PB11);
		}
		uartCircularQueueReInit(&uart3);
	}

	/* Enable Interrupt */
	if (useINT == ENABLE)
	{
		USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
		USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
		if ((uint32_t) USARTx == (uint32_t) USART1)
		{
			HAL_NVIC_Config(USART1_IRQn, NVIC_PriorityGroup_0, 0);
			uart1.useINT = ENABLE;
		}
		else if ((uint32_t) USARTx == (uint32_t) USART2)
		{
			HAL_NVIC_Config(USART2_IRQn, NVIC_PriorityGroup_0, 0);
			uart2.useINT = ENABLE;
		}
		else if ((uint32_t) USARTx == (uint32_t) USART3)
		{
			HAL_NVIC_Config(USART3_IRQn, NVIC_PriorityGroup_0, 0);
			uart3.useINT = ENABLE;
		}
	}

	/* USART configuration */
	USART_Init(USARTx, getUSARTConfig());
	/* Enable USART */
	USART_Cmd(USARTx, ENABLE);
}

int8_t ochar(USART_TypeDef *USARTx,uint8_t c)
{
	if ((uint32_t) USARTx == (uint32_t) USART1)
	{
		if (uart1.useINT == ENABLE)
		{
			if (USARTPutToQueueData(&uart1, c) < 0)
				return -1;
		}
		else
		{
			USARTSendByte(USART1, c);
		}
	}
	else if ((uint32_t) USARTx == (uint32_t) USART2)
	{
		if (uart2.useINT == ENABLE)
		{
			if (USARTPutToQueueData(&uart2, c) < 0)
				return -1;
		}
		else
		{
			USARTSendByte(USART2, c);
		}
	}
	return 0;
}

int16_t ichar(USART_TypeDef *USARTx)
{
	if ((uint32_t) USARTx == (uint32_t) USART1)
	{
		if (uart1.useINT == ENABLE)
			return USARTGetFromQueueData(&uart1);
		else
			return USARTRecevieByte(USART1);
	}
	else if ((uint32_t) USARTx == (uint32_t) USART2)
	{
		if (uart2.useINT == ENABLE)
			return USARTGetFromQueueData(&uart2);
		else
			return USARTRecevieByte(USART2);
	}
	return -1;
}

static void USARTPinInit(const GPIOx_TypeDef *Tx,const GPIOx_TypeDef *Rx)
{
	HAL_GPIO_Init(Tx,GPIO_Mode_AF_PP);			/* Tx */
	HAL_GPIO_Init(Rx,GPIO_Mode_IN_FLOATING);	/* Rx */
}

static void USARTSendByte(USART_TypeDef *USARTx,uint8_t c)
{
	USART_SendData(USARTx, c);
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
}

static int16_t USARTRecevieByte(USART_TypeDef *USARTx)
{
	if(USART_GetFlagStatus(USARTx,USART_FLAG_RXNE) != RESET)
	{
		return USART_ReceiveData(USARTx);
	}
	else
		return -1;
}

uint8_t uartRxTimeOut(UART_TypeDef *uartx)
{
	if(HAL_Timer_GetSystick() - uartx->timeOut > Delay_3Sec)
	{
		return 1;
	}
	return 0;
}

static int16_t USARTGetFromQueueData(UART_TypeDef *uartx)
{
	int16_t c;
	if(uartx->uartRx.front != uartx->uartRx.rear)
	{
		c = uartx->uartRx.buf[uartx->uartRx.front++];
		if(uartx->uartRx.front == QUEUE_LEN)
		{
			uartx->uartRx.front=0;
		}
		return c;
	}
	else
		return -1;
}

static int8_t USARTPutToQueueData(UART_TypeDef *uartx,uint8_t c)
{
	uint8_t tx;

	tx = uartx->uartTx.rear;
	tx++;
	if(tx == QUEUE_LEN)
	{
		tx = 0;
	}
	DisableAllInterrupt();
	if(tx != uartx->uartTx.front)
	{
		uartx->uartTx.buf[uartx->uartTx.rear] = c;
		uartx->uartTx.rear = tx;

		if(uartx->flag.tranRunning == 0)
		{
			if(uartx == &uart1)
			{
				USART_ITConfig(USART1,USART_IT_TXE,ENABLE);
			}
			else if(uartx == &uart2)
			{
				USART_ITConfig(USART2,USART_IT_TXE,ENABLE);
			}
			uartx->flag.tranRunning = 1;
		}
		EnableAllInterrupt();
		return 0;
	}
	else
	{
		EnableAllInterrupt();
		return -1;
	}
}

static void USARTIrqRoutine(USART_TypeDef *USARTx, UART_TypeDef *uartx)
{
	uint8_t rx;
	if(USART_GetITStatus(USARTx, USART_IT_RXNE) != RESET)
	{
		rx = uartx->uartRx.rear;
		rx++;
		if(rx == QUEUE_LEN)
		{
			rx=0;
		}
		if(rx != uartx->uartRx.front)
		{
			uartx->uartRx.buf[uartx->uartRx.rear] = USART_ReceiveData(USARTx);
			uartx->uartRx.rear=rx;
//			uartx->timeOut = HAL_Timer_GetSystick();
		}
		USART_ClearITPendingBit(USARTx, USART_IT_RXNE);
	}

	if(USART_GetITStatus(USARTx, USART_IT_TXE) != RESET)
	{
		if(uartx->uartTx.front != uartx->uartTx.rear)
		{
			USART_SendData(USARTx, uartx->uartTx.buf[uartx->uartTx.front]);
			uartx->uartTx.front++;
			if(uartx->uartTx.front == QUEUE_LEN)
			{
				uartx->uartTx.front = 0;
			}
		}
		if(uartx->uartTx.front == uartx->uartTx.rear)
		{
			USART_ClearITPendingBit(USARTx, USART_IT_TXE);
			USART_ITConfig(USARTx, USART_IT_TXE, DISABLE);
			USART_ITConfig(USARTx, USART_IT_TC, ENABLE);
			uartx->flag.tranRunning = 0;
		}
	}

	if(USART_GetITStatus(USARTx, USART_IT_TC) != RESET)
	{
		USART_ClearITPendingBit(USARTx, USART_IT_TC);
		USART_ITConfig(USARTx, USART_IT_TC, DISABLE);
	}
}

/*************USART Interrupt******************************/
/**
  * @brief  This function handles USART1 global interrupt request.
  * @param  None
  * @retval None
  */
void USART1_IRQHandler(void)
{
	USARTIrqRoutine(USART1,&uart1);
}

/**
  * @brief  This function handles USART2 global interrupt request.
  * @param  None
  * @retval None
  */
void USART2_IRQHandler(void)
{
	USARTIrqRoutine(USART2,&uart2);
}

/**
  * @brief  This function handles USART3 global interrupt request.
  * @param  None
  * @retval None
  */
void USART3_IRQHandler(void)
{
	USARTIrqRoutine(USART3,&uart3);
}
