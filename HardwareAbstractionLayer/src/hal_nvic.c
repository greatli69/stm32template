/*
 * hal_nvic.c
 *
 *  Created on: 2015�~6��9��
 *      Author: Thomas
 */
#include "hal_nvic.h"

void HAL_NVIC_Config(uint8_t IRQChannel,uint32_t NVIC_PriorityGroup,uint8_t SubPriority)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Configure the NVIC Preemption Priority Bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup);

	NVIC_InitStructure.NVIC_IRQChannel = IRQChannel;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = SubPriority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
