/*
 * hal_adc.c
 *
 *  Created on: 2015�~6��18��
 *      Author: Thomas
 */
#include "hal_adc.h"
#include "hal_gpio.h"
#include "hal_dma.h"
#include "hal_timer.h"
#define ADC1_DR_Address    ((uint32_t)0x4001244C)

__IO uint16_t ADC1RawValue;

void HAL_ADC_GetADC1RawData(uint16_t *rawData)
{
	if((DMA_GetFlagStatus(DMA1_FLAG_TC1)) !=RESET){
		DMA_ClearFlag(DMA1_FLAG_TC1);
		*rawData = ADC1RawValue;
	}
}

int8_t HAL_ADC1_Init(const GPIOx_TypeDef *PINx)
{
	ADC_InitTypeDef ADC_InitStructure;

	if(PINx->adcChannel == NO_ADC_CHANNEL)	return -1;

	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);

	/* GPIO Init */
	HAL_GPIO_Init(PINx,GPIO_Mode_AIN);
	HAL_DMA_Init(DMA1_Channel1, ADC1_DR_Address, &ADC1RawValue);

	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_RegularChannelConfig(ADC1,PINx->adcChannel,1,ADC_SAMPLE_TIME);

	ADC_DMACmd(ADC1, ENABLE);
	ADC_Cmd(ADC1, ENABLE);

	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));

	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

	Delay(Delay_1MS);	/* wait ADC stable */

	return 0;
}
