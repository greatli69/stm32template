/*
 * hal_timer.c
 *
 *  Created on: 2015年6月4日
 *      Author: Thomas
 */
/* Includes ------------------------------------------------------------------*/
#include "hal_timer.h"
#include "stm32f10x_it.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint32_t systick;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Initialize systick
 * @param  setTime:
 * @retval None
 */
void HAL_Timer_InitSysTick(SysTickSetTime_TypeDef setTime)
{
  if (SysTick_Config(SystemCoreClock / setTime))
  {
	/* Capture error */
	while (1);
  }
}

void HAL_Timer_TIMBase_Init(TIM_TypeDef *TIMx,TIMSetTime_TypeDef setTime)
{
	  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	  /* TIM2 configuration */
	  TIM_TimeBaseStructure.TIM_Period = setTime;
	  TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock/12000) - 1);
	  TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;
	  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	  TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);

	  TIM_PrescalerConfig(TIMx, ((SystemCoreClock/12000) - 1), TIM_PSCReloadMode_Immediate);

	  TIM_ClearFlag(TIMx, TIM_FLAG_Update);

	  if(TIMx == TIM2)
		  HAL_NVIC_Config(TIM2_IRQn,NVIC_PriorityGroup_0,0);
	  else if(TIMx == TIM3)
		  HAL_NVIC_Config(TIM3_IRQn,NVIC_PriorityGroup_0,0);
	  else if(TIMx == TIM4)
		  HAL_NVIC_Config(TIM4_IRQn,NVIC_PriorityGroup_0,0);

	  TIM_ITConfig(TIMx, TIM_IT_Update, ENABLE);

	  TIM_Cmd(TIMx, ENABLE);
}

uint32_t HAL_Timer_GetSystick(void)
{
	return systick;
}

/**
  * @brief  systick mutually reduce with userTimer.The value compare with compareValue
  * @param  userTimer:
  * @param  compareValue:
  * @retval 1 = 相減值大於compareValue;0 = 相減值等於compareValue;-1 = 相減值小於compareValue
  */
int8_t HAL_Timer_SystickComparison(uint32_t userTimer,uint32_t compareValue)
{
	if((systick - userTimer) > compareValue)
		return 1;
	else if((systick - userTimer) == compareValue)
		return 0;
	else
		return -1;
}

/***********************Interrupt***************************/
/**
  * @brief  This function handles SysTick Handler. 1ms trigger.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	systick++;
}
