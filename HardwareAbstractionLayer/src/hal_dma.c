/*
 * hal_dma.c
 *
 *  Created on: 2015�~6��18��
 *      Author: Thomas
 */
#include "hal_dma.h"

DMA_InitTypeDef MyDMA_InitStructure;

void HAL_DMA_Init(DMA_Channel_TypeDef *DMAx_Channel,uint32_t DMA_PeripheralBaseAddr,volatile uint16_t *DMA_MemoryBaseAddr)
{
	/* DMA1 channel1 configuration ----------------------------------------------*/
	DMA_DeInit(DMAx_Channel);
	MyDMA_InitStructure.DMA_PeripheralBaseAddr = DMA_PeripheralBaseAddr;
	MyDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_MemoryBaseAddr;
	MyDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	MyDMA_InitStructure.DMA_BufferSize = 1;
	MyDMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	MyDMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	MyDMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	MyDMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	MyDMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	MyDMA_InitStructure.DMA_Priority = DMA_Priority_High;
	MyDMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMAx_Channel, &MyDMA_InitStructure);

	DMA_Cmd(DMAx_Channel, ENABLE);
}
