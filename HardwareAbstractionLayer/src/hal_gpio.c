/*
 * hal_gpio.c
 *
 *  Created on: 2015�~6��2��
 *      Author: Thomas
 */
/* Includes ------------------------------------------------------------------*/
#include "hal_gpio.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private consts ------------------------------------------------------------*/
const GPIOx_TypeDef CPA0={GPIO_Pin_0,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource0,ADC_Channel_0},*PA0=&CPA0;
const GPIOx_TypeDef CPA1={GPIO_Pin_1,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource1,ADC_Channel_1},*PA1=&CPA1;
const GPIOx_TypeDef CPA2={GPIO_Pin_2,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource2,ADC_Channel_2},*PA2=&CPA2;
const GPIOx_TypeDef CPA3={GPIO_Pin_3,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource3,ADC_Channel_3},*PA3=&CPA3;
const GPIOx_TypeDef CPA4={GPIO_Pin_4,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource4,ADC_Channel_4},*PA4=&CPA4;
const GPIOx_TypeDef CPA5={GPIO_Pin_5,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource5,ADC_Channel_5},*PA5=&CPA5;
const GPIOx_TypeDef CPA6={GPIO_Pin_6,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource6,ADC_Channel_6},*PA6=&CPA6;
const GPIOx_TypeDef CPA7={GPIO_Pin_7,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource7,ADC_Channel_7},*PA7=&CPA7;
const GPIOx_TypeDef CPA8={GPIO_Pin_8,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource8,NO_ADC_CHANNEL},*PA8=&CPA8;
const GPIOx_TypeDef CPA9={GPIO_Pin_9,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource9,NO_ADC_CHANNEL},*PA9=&CPA9;
const GPIOx_TypeDef CPA10={GPIO_Pin_10,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource10,NO_ADC_CHANNEL},*PA10=&CPA10;
const GPIOx_TypeDef CPA11={GPIO_Pin_11,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource11,NO_ADC_CHANNEL},*PA11=&CPA11;
const GPIOx_TypeDef CPA12={GPIO_Pin_12,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource12,NO_ADC_CHANNEL},*PA12=&CPA12;
const GPIOx_TypeDef CPA13={GPIO_Pin_13,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource13,NO_ADC_CHANNEL},*PA13=&CPA13;
const GPIOx_TypeDef CPA14={GPIO_Pin_14,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource14,NO_ADC_CHANNEL},*PA14=&CPA14;
const GPIOx_TypeDef CPA15={GPIO_Pin_15,GPIOA,GPIO_PortSourceGPIOA,GPIO_PinSource15,NO_ADC_CHANNEL},*PA15=&CPA15;

const GPIOx_TypeDef CPB0={GPIO_Pin_0,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource0,ADC_Channel_8},*PB0=&CPB0;
const GPIOx_TypeDef CPB1={GPIO_Pin_1,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource1,ADC_Channel_9},*PB1=&CPB1;
const GPIOx_TypeDef CPB2={GPIO_Pin_2,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource2,NO_ADC_CHANNEL},*PB2=&CPB2;
const GPIOx_TypeDef CPB3={GPIO_Pin_3,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource3,NO_ADC_CHANNEL},*PB3=&CPB3;
const GPIOx_TypeDef CPB4={GPIO_Pin_4,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource4,NO_ADC_CHANNEL},*PB4=&CPB4;
const GPIOx_TypeDef CPB5={GPIO_Pin_5,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource5,NO_ADC_CHANNEL},*PB5=&CPB5;
const GPIOx_TypeDef CPB6={GPIO_Pin_6,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource6,NO_ADC_CHANNEL},*PB6=&CPB6;
const GPIOx_TypeDef CPB7={GPIO_Pin_7,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource7,NO_ADC_CHANNEL},*PB7=&CPB7;
const GPIOx_TypeDef CPB8={GPIO_Pin_8,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource8,NO_ADC_CHANNEL},*PB8=&CPB8;
const GPIOx_TypeDef CPB9={GPIO_Pin_9,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource9,NO_ADC_CHANNEL},*PB9=&CPB9;
const GPIOx_TypeDef CPB10={GPIO_Pin_10,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource10},*PB10=&CPB10;
const GPIOx_TypeDef CPB11={GPIO_Pin_11,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource11},*PB11=&CPB11;
const GPIOx_TypeDef CPB12={GPIO_Pin_12,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource12},*PB12=&CPB12;
const GPIOx_TypeDef CPB13={GPIO_Pin_13,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource13},*PB13=&CPB13;
const GPIOx_TypeDef CPB14={GPIO_Pin_14,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource14},*PB14=&CPB14;
const GPIOx_TypeDef CPB15={GPIO_Pin_15,GPIOB,GPIO_PortSourceGPIOB,GPIO_PinSource15},*PB15=&CPB15;

const GPIOx_TypeDef CPC0={GPIO_Pin_0,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource0,ADC_Channel_10},*PC0=&CPC0;
const GPIOx_TypeDef CPC1={GPIO_Pin_1,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource1,ADC_Channel_11},*PC1=&CPC1;
const GPIOx_TypeDef CPC2={GPIO_Pin_2,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource2,ADC_Channel_12},*PC2=&CPC2;
const GPIOx_TypeDef CPC3={GPIO_Pin_3,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource3,ADC_Channel_13},*PC3=&CPC3;
const GPIOx_TypeDef CPC4={GPIO_Pin_4,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource4,ADC_Channel_14},*PC4=&CPC4;
const GPIOx_TypeDef CPC5={GPIO_Pin_5,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource5,ADC_Channel_15},*PC5=&CPC5;
const GPIOx_TypeDef CPC6={GPIO_Pin_6,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource6,NO_ADC_CHANNEL},*PC6=&CPC6;
const GPIOx_TypeDef CPC7={GPIO_Pin_7,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource7,NO_ADC_CHANNEL},*PC7=&CPC7;
const GPIOx_TypeDef CPC8={GPIO_Pin_8,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource8,NO_ADC_CHANNEL},*PC8=&CPC8;
const GPIOx_TypeDef CPC9={GPIO_Pin_9,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource9,NO_ADC_CHANNEL},*PC9=&CPC9;
const GPIOx_TypeDef CPC10={GPIO_Pin_10,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource10,NO_ADC_CHANNEL},*PC10=&CPC10;
const GPIOx_TypeDef CPC11={GPIO_Pin_11,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource11,NO_ADC_CHANNEL},*PC11=&CPC11;
const GPIOx_TypeDef CPC12={GPIO_Pin_12,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource12,NO_ADC_CHANNEL},*PC12=&CPC12;
const GPIOx_TypeDef CPC13={GPIO_Pin_13,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource13,NO_ADC_CHANNEL},*PC13=&CPC13;
const GPIOx_TypeDef CPC14={GPIO_Pin_14,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource14,NO_ADC_CHANNEL},*PC14=&CPC14;
const GPIOx_TypeDef CPC15={GPIO_Pin_15,GPIOC,GPIO_PortSourceGPIOC,GPIO_PinSource15,NO_ADC_CHANNEL},*PC15=&CPC15;

const GPIOx_TypeDef CPD0={GPIO_Pin_0,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource0,NO_ADC_CHANNEL},*PD0=&CPD0;
const GPIOx_TypeDef CPD1={GPIO_Pin_1,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource1,NO_ADC_CHANNEL},*PD1=&CPD1;
const GPIOx_TypeDef CPD2={GPIO_Pin_2,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource2,NO_ADC_CHANNEL},*PD2=&CPD2;
const GPIOx_TypeDef CPD3={GPIO_Pin_3,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource3,NO_ADC_CHANNEL},*PD3=&CPD3;
const GPIOx_TypeDef CPD4={GPIO_Pin_4,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource4,NO_ADC_CHANNEL},*PD4=&CPD4;
const GPIOx_TypeDef CPD5={GPIO_Pin_5,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource5,NO_ADC_CHANNEL},*PD5=&CPD5;
const GPIOx_TypeDef CPD6={GPIO_Pin_6,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource6,NO_ADC_CHANNEL},*PD6=&CPD6;
const GPIOx_TypeDef CPD7={GPIO_Pin_7,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource7,NO_ADC_CHANNEL},*PD7=&CPD7;
const GPIOx_TypeDef CPD8={GPIO_Pin_8,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource8,NO_ADC_CHANNEL},*PD8=&CPD8;

const GPIOx_TypeDef CPD9={GPIO_Pin_9,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource9,NO_ADC_CHANNEL},*PD9=&CPD9;
const GPIOx_TypeDef CPD10={GPIO_Pin_10,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource10,NO_ADC_CHANNEL},*PD10=&CPD10;
const GPIOx_TypeDef CPD11={GPIO_Pin_11,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource11,NO_ADC_CHANNEL},*PD11=&CPD11;
const GPIOx_TypeDef CPD12={GPIO_Pin_12,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource12,NO_ADC_CHANNEL},*PD12=&CPD12;
const GPIOx_TypeDef CPD13={GPIO_Pin_13,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource13,NO_ADC_CHANNEL},*PD13=&CPD13;
const GPIOx_TypeDef CPD14={GPIO_Pin_14,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource14,NO_ADC_CHANNEL},*PD14=&CPD14;
const GPIOx_TypeDef CPD15={GPIO_Pin_15,GPIOD,GPIO_PortSourceGPIOD,GPIO_PinSource15,NO_ADC_CHANNEL},*PD15=&CPD15;

void HAL_GPIO_EXTI_Init(const GPIOx_TypeDef  *GPIOx,const EXTITrigger_TypeDef Trigger_Mode)
{
	EXTI_InitTypeDef   EXTI_InitStructure;

	/* Enable AFIO clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	HAL_GPIO_Init(GPIOx,GPIO_Mode_IN_FLOATING);
	GPIO_EXTILineConfig(GPIOx->gpioPortSource, GPIOx->gpioPinSource);

	switch(GPIOx->gpioPin){
	case GPIO_Pin_0:
		EXTI_InitStructure.EXTI_Line = EXTI_Line0;
		HAL_NVIC_Config(EXTI0_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_1:
		EXTI_InitStructure.EXTI_Line = EXTI_Line1;
		HAL_NVIC_Config(EXTI1_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_2:
		EXTI_InitStructure.EXTI_Line = EXTI_Line2;
		HAL_NVIC_Config(EXTI2_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_3:
		EXTI_InitStructure.EXTI_Line = EXTI_Line3;
		HAL_NVIC_Config(EXTI3_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_4:
		EXTI_InitStructure.EXTI_Line = EXTI_Line4;
		HAL_NVIC_Config(EXTI4_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_5:
		EXTI_InitStructure.EXTI_Line = EXTI_Line5;
		HAL_NVIC_Config(EXTI9_5_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_6:
		EXTI_InitStructure.EXTI_Line = EXTI_Line6;
		HAL_NVIC_Config(EXTI9_5_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_7:
		EXTI_InitStructure.EXTI_Line = EXTI_Line7;
		HAL_NVIC_Config(EXTI9_5_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_8:
		EXTI_InitStructure.EXTI_Line = EXTI_Line8;
		HAL_NVIC_Config(EXTI9_5_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_9:
		EXTI_InitStructure.EXTI_Line = EXTI_Line9;
		HAL_NVIC_Config(EXTI9_5_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_10:
		EXTI_InitStructure.EXTI_Line = EXTI_Line10;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_11:
		EXTI_InitStructure.EXTI_Line = EXTI_Line11;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_12:
		EXTI_InitStructure.EXTI_Line = EXTI_Line12;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_13:
		EXTI_InitStructure.EXTI_Line = EXTI_Line13;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_14:
		EXTI_InitStructure.EXTI_Line = EXTI_Line14;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	case GPIO_Pin_15:
		EXTI_InitStructure.EXTI_Line = EXTI_Line15;
		HAL_NVIC_Config(EXTI15_10_IRQn,NVIC_PriorityGroup_0,0x0F);
		break;
	}
	/* Configure EXTI line */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = Trigger_Mode;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
}

uint8_t HAL_GPIO_Init(const GPIOx_TypeDef  *GPIOx, GPIOMode_TypeDef GPIO_MODE)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	/* Enable the GPIO  Clock */
	if(GPIOx->gpioPort == GPIOA){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}else if(GPIOx->gpioPort == GPIOB){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}else if(GPIOx->gpioPort == GPIOC){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}else if(GPIOx->gpioPort == GPIOD){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	}else return ERROR;

	GPIO_InitStructure.GPIO_Pin = GPIOx->gpioPin;
	GPIO_InitStructure.GPIO_Mode = GPIO_MODE;
	if(GPIO_MODE == GPIO_Mode_IN_FLOATING){

	}else{
		GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_SETTING;
	}
	GPIO_Init(GPIOx->gpioPort, &GPIO_InitStructure);

	return SUCCESS;
}

uint8_t HAL_GPIO_ReadPin(const GPIOx_TypeDef  *GPIOx)
{
	return GPIO_ReadInputDataBit(GPIOx->gpioPort,GPIOx->gpioPin);
}

void HAL_GPIO_SetON(const GPIOx_TypeDef  *GPIOx)
{
	(GPIOx->gpioPort)->BSRR = GPIOx->gpioPin;
}

void HAL_GPIO_SetOFF(const GPIOx_TypeDef  *GPIOx)
{
	(GPIOx->gpioPort)->BRR = GPIOx->gpioPin;
}

void HAL_GPIO_SetJTAGDPAsGpio(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	/* Disable JTAG-DP and setting as GPIO */
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

    /* Configure PB.03 (JTDO) and PB.04 (JTRST) and PA.15 (JTDI) as
       output push-pull */
	HAL_GPIO_Init(PB3,GPIO_Mode_Out_PP);
	HAL_GPIO_Init(PA15,GPIO_Mode_Out_PP);
	HAL_GPIO_Init(PB4,GPIO_Mode_Out_PP);
}

void HAL_GPIO_MCOEnable(void)
{
	HAL_GPIO_Init(PA8,GPIO_Mode_AF_PP);
	RCC_MCOConfig(RCC_MCO_HSE);
}

void HAL_GPIO_InitGpio_PP(const GPIOx_TypeDef  *GPIOx)
{
	HAL_GPIO_Init(GPIOx,GPIO_Mode_Out_PP);
}

void HAL_GPIO_InitGpio_OD(const GPIOx_TypeDef  *GPIOx)
{
	HAL_GPIO_Init(GPIOx,GPIO_Mode_Out_OD);
}

void HAL_GPIO_InitGpio_Floating(const GPIOx_TypeDef  *GPIOx)
{
	HAL_GPIO_Init(GPIOx,GPIO_Mode_IN_FLOATING);
}
