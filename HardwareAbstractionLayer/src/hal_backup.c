/*
 * hal_backup.c
 *
 *  Created on: 2015�~7��6��
 *      Author: Thomas
 */

#include "hal_backup.h"

/**
 * @brief  Initiate Backup Register
 * @param  None
 * @retval None
 */
void HAL_BackupRegister_Init(void)
{
	  /* Enable PWR and BKP clock */
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	  /* Enable write access to Backup domain */
	  PWR_BackupAccessCmd(ENABLE);

	  /* Clear Tamper pin Event(TE) pending flag */
	  BKP_ClearFlag();
}

void HAL_BackupRegister_WriteHalfWord(BKPADDR_TypeDef addr, uint16_t Data)
{
	BKP_WriteBackupRegister(addr, Data);
}

uint16_t HAL_BackupRegister_ReadHalfWord(BKPADDR_TypeDef addr)
{
	return BKP_ReadBackupRegister(addr);
}
