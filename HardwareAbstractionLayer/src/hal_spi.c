/*
 * hal_spi.c
 *
 *  Created on: 2015�~6��8��
 *      Author: Thomas
 */
#include "hal_spi.h"

void HAL_SPI_Init(SPI_TypeDef *SPIx,const GPIOx_TypeDef *CSPin, uint8_t enableRemap)
{
	SPI_InitTypeDef    SPI_InitStructure;

	/* Enable SPI clock  */
	if(SPIx == SPI1){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
		if(enableRemap == ENABLE){

		}else{

		}
	}
	else if(SPIx == SPI2){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
		if(enableRemap == ENABLE){

		}else{

		}
	}
	else if(SPIx == SPI3){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
		if(enableRemap == ENABLE){
			/* Enable GPIO clock */
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
			GPIO_PinRemapConfig(GPIO_Remap_SPI3, ENABLE);

			HAL_GPIO_Init(PC10,GPIO_Mode_AF_PP);	/* SCK */
			HAL_GPIO_Init(PC11,GPIO_Mode_AF_PP);	/* MISO */
			HAL_GPIO_Init(PC12,GPIO_Mode_AF_PP);	/* MOSI */
		}else{
			HAL_GPIO_Init(PB3,GPIO_Mode_AF_PP);	/* SCK */
			HAL_GPIO_Init(PB4,GPIO_Mode_AF_PP);	/* MISO */
			HAL_GPIO_Init(PB5,GPIO_Mode_AF_PP);	/* MOSI */
		}
	}

	/* Configure NCS in Output Push-Pull mode */
	HAL_GPIO_Init(CSPin,GPIO_Mode_Out_PP);	/* CS */

	SPI_I2S_DeInit(SPIx);
	/* SPI Config */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_Init(SPIx, &SPI_InitStructure);
	/* SPI enable */
	SPI_Cmd(SPIx, ENABLE);
}

/**
  * @brief  Transmits a Data through the SPIx/I2Sx peripheral.
  * @param  SPIx: where x can be
  *   - 1, 2 or 3 in SPI mode
  *   - 2 or 3 in I2S mode
  * @param  Data : Data to be transmitted.
  * @retval None
  */
void HAL_SPI_SendByte(SPI_TypeDef* SPIx, uint16_t Data)
{
	SPIx->DR = Data;
	while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_BSY) != RESET)
	{
	}
}
