/*
 * hal_adc.h
 *
 *  Created on: 2015�~6��18��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_ADC_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_ADC_H_
#include "stm32f10x.h"
#include "hal_gpio.h"

void HAL_ADC_GetADC1RawData(uint16_t *rawData);
int8_t HAL_ADC1_Init(const GPIOx_TypeDef *PINx);


#define ADC_SAMPLE_TIME ADC_SampleTime_55Cycles5 /* 55Cycles */
#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_ADC_H_ */
