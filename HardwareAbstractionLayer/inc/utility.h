/*
 * utility.h
 *
 *  Created on: 2015�~6��8��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_UTILITY_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_UTILITY_H_
#include <stdio.h>
#include <ctype.h>
#include "stm32f10x.h"

/* Exported types ------------------------------------------------------------*/
#define EnableAllInterrupt()	__enable_irq()
#define DisableAllInterrupt()	__disable_irq()

#define APPLICATION_COMM_PORT	USART1
#define DEBUG_PORT				USART2
/* Common routines */
#define IS_AF(c)  ((c >= 'A') && (c <= 'F'))
#define IS_af(c)  ((c >= 'a') && (c <= 'f'))
#define IS_09(c)  ((c >= '0') && (c <= '9'))
#define ISVALIDHEX(c)  IS_AF(c) || IS_af(c) || IS_09(c)
#define ISVALIDDEC(c)  IS_09(c)
#define CONVERTDEC(c)  (c - '0')

#define CONVERTHEX_alpha(c)  (IS_AF(c) ? (c - 'A'+10) : (c - 'a'+10))
#define CONVERTHEX(c)   (IS_09(c) ? (c - '0') : CONVERTHEX_alpha(c))

#define	VOID_CAST(x)	(*((unsigned char **)&x))

#define clearBuff(block , size)     pattern(block , 0, size)
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
extern uint32_t ChipUniqueID[3];
extern const uint16_t crc16tab[256];
/* Exported functions ------------------------------------------------------- */
void Delay(uint32_t nTime);
uint8_t getChipID(uint32_t (*chipUniqueID)[3]);
int8_t switchLettersUppercase(unsigned char *str);
uint32_t Str2Int(uint8_t *inputstr, int32_t *intnum);
void pattern(void *block, uint8_t c, uint16_t size);
void imovblk (const void *sur, void *des, int16_t len);
int8_t cmpblk(const void *sur, const void *des, int8_t len);
int8_t convertDecToAscii(uint8_t c);
void Int2Str(uint8_t* str, int32_t intnum);
void strCat(char* str1, const char* str2);
uint32_t strLen(const char *pStr);
uint8_t * findChar(uint8_t pStr[],const uint8_t c , const uint8_t pos);
uint16_t crc16_ccitt(const uint8_t *buf, int32_t len);
uint32_t checkCRCResult(const uint8_t *buf, uint32_t crcAddr);
#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_UTILITY_H_ */
