/*
 * hal_dma.h
 *
 *  Created on: 2015�~6��18��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_DMA_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_DMA_H_
#include "stm32f10x.h"

void HAL_DMA_Init(DMA_Channel_TypeDef *DMAx_Channel,uint32_t DMA_PeripheralBaseAddr,volatile uint16_t *DMA_MemoryBaseAddr);

#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_DMA_H_ */
