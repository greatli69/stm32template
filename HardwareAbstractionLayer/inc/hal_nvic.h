/*
 * hal_nvic.h
 *
 *  Created on: 2015�~6��9��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_NVIC_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_NVIC_H_
#include "stm32f10x.h"

void HAL_NVIC_Config(uint8_t IRQChannel,uint32_t NVIC_PriorityGroup,uint8_t SubPriority);

#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_NVIC_H_ */
