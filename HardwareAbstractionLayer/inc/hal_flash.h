/*
 * hal_flash.h
 *
 *  Created on: 2015�~6��12��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_FLASH_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_FLASH_H_
#include "stm32f10x.h"

/* Define the STM32F10x FLASH Page Size depending on the used STM32 device */
#if defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_CL) || defined (STM32F10X_XL)
  #define FLASH_PAGE_SIZE    ((uint16_t)0x800)
#else
  #define FLASH_PAGE_SIZE    ((uint16_t)0x400)
#endif

void HAL_FLASH_EarseOnePage(uint32_t startAdrr);
int8_t HAL_FLASH_ProgramHalfWordData(uint32_t startAdrr,uint16_t Data);
uint16_t HAL_FLASH_GetHalfWordData(uint32_t startAdrr);
int8_t HAL_FLASH_CancelWriteProtectionPage(uint32_t FLASH_WRProt_Pages);
int8_t HAL_FLASH_SetWriteProtectionPage(uint32_t FLASH_WRProt_Pages);
int8_t HAL_FLASH_EarsePage(uint32_t startAdrr, uint32_t endAdrr);

#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_FLASH_H_ */
