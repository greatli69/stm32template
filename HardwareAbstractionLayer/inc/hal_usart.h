/*
 * hal_usart.h
 *
 *  Created on: 2015�~6��4��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_USART_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_USART_H_
#include "stm32f10x.h"
#include "hal_gpio.h"
#include "stm32_hal.h"

#define QUEUE_LEN	255
#define PACKET_LEN	1028
typedef struct{
	uint8_t buf[PACKET_LEN];
	uint8_t front,rear;
}CircularQueue_TypeDef;

typedef struct{
	CircularQueue_TypeDef uartTx;
	CircularQueue_TypeDef uartRx;
	struct{
		uint8_t tranRunning: 1;
		uint8_t recvData: 1;
	}flag;
	uint32_t timeOut;
	uint8_t useINT;
}UART_TypeDef;

extern UART_TypeDef uart1,uart2,uart3;

#define SerialPutString(Uartx,x) Serial_PutString(Uartx, (uint8_t*)(x))

void HAL_USART_USART1_USE_CTS_RTS_Init(void);
void HAL_USART_Init(USART_TypeDef *USARTx,uint8_t enableRemap,FunctionalState useINT);
void HAL_USART_USARTConfig(uint32_t baudrate,int8_t odevity,uint8_t length,int8_t stopbit);
USART_InitTypeDef *getUSARTConfig(void);
uint8_t uartRxTimeOut(UART_TypeDef *uartx);
int8_t ochar(USART_TypeDef *USARTx,uint8_t c);
int16_t ichar(USART_TypeDef *USARTx);
void Serial_PutString(USART_TypeDef *USARTx, uint8_t *s);

#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_USART_H_ */
