/*
 * hal_backup.h
 *
 *  Created on: 2015�~7��6��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_BACKUP_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_BACKUP_H_
#include "stm32f10x.h"
#include "stm32f10x_bkp.h"

typedef enum{
	BkpDataAddr1 = BKP_DR1,
	BkpDataAddr2 = BKP_DR2,
}BKPADDR_TypeDef;

void HAL_BackupRegister_Init(void);
void HAL_BackupRegister_WriteHalfWord(BKPADDR_TypeDef addr, uint16_t Data);
uint16_t HAL_BackupRegister_ReadHalfWord(BKPADDR_TypeDef addr);

#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_BACKUP_H_ */
