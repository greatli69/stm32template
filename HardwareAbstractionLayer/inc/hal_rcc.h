/*
 * hal_rcc.h
 *
 *  Created on: 2015�~6��9��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_RCC_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_RCC_H_
#include "stm32f10x.h"
#include "hal_nvic.h"

extern uint8_t rccFlag[];
#define SET_RESET_SOURCE_NRST	rccFlag[0]|=0x01
#define CLR_RESET_SOURCE_NRST	rccFlag[0]=rccFlag[0]&0xFE
#define IS_RESET_SOURCE_NRST	(rccFlag[0]&0x01)

#define SET_RESET_SOURCE_SOFTWARE	rccFlag[0]|=0x02
#define CLR_RESET_SOURCE_SOFTWARE	rccFlag[0]=rccFlag[0]&0xFD
#define IS_RESET_SOURCE_SOFTWARE	(rccFlag[0]&0x02)

#define SET_RESET_SOURCE_POR	rccFlag[0]|=0x04
#define CLR_RESET_SOURCE_POR	rccFlag[0]=rccFlag[0]&0xFB
#define IS_RESET_SOURCE_POR		(rccFlag[0]&0x04)

void HAL_RCC_CSSENABLE(void);
void getResetSourceFlag(void);
#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_RCC_H_ */
