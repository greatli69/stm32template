/*
 * hal_i2c.h
 *
 *  Created on: 2015�~6��15��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_I2C_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_I2C_H_
#include "stm32f10x.h"
extern DMA_InitTypeDef   sEEDMA_InitStructure;

#define I2C_CLOCKSPEED	100000	/* 100kHz */
#define I2C_MASTER_ADDR	0x00

#define EEPROM_ADDRESS	0xA0

void HAL_I2C_Init(I2C_TypeDef *I2Cx);

#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_I2C_H_ */
