/*
 * hal_timer.h
 *
 *  Created on: 2015�~6��4��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_TIMER_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_TIMER_H_
/* Includes ------------------------------------------------------------------*/
#include "stm32_hal.h"
#include "hal_usart.h"
/* Exported types ------------------------------------------------------------*/
typedef enum{
	SET_SYSTICK_1MS=1000,
	SET_SYSTICK_5MS=200,
	SET_SYSTICK_10MS=100,
	SET_SYSTICK_20MS=50,
	SET_SYSTICK_50MS=20
}SysTickSetTime_TypeDef;

typedef enum{
	TIM_1MS=2,
	TIM_10MS=20,
	TIM_50MS=100,
	TIM_100MS=200,
	TIM_500MS=1000,
	TIM_1S=2000
}TIMSetTime_TypeDef;


typedef enum{
	Delay_1MS=1,
	Delay_5MS=5,
	Delay_10MS=10,
	Delay_20MS=20,
	Delay_50MS=50,
	Delay_100MS=100,
	Delay_200MS=200,
	Delay_500MS=500,
	Delay_1Sec=1000,
	Delay_3Sec=3000,
	Delay_4Sec=4000,
}DelayTime_TypeDef;

/* Exported constants --------------------------------------------------------*/
extern volatile uint32_t systick;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void HAL_Timer_InitSysTick(SysTickSetTime_TypeDef setTime);
void HAL_Timer_TIMBase_Init(TIM_TypeDef *TIMx,TIMSetTime_TypeDef setTime);
uint32_t HAL_Timer_GetSystick(void);
int8_t HAL_Timer_SystickComparison(uint32_t userTimer,uint32_t compareValue);

#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_TIMER_H_ */
