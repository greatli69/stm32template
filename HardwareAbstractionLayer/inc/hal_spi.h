/*
 * hal_spi.h
 *
 *  Created on: 2015�~6��8��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_SPI_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_SPI_H_
//#include "stm32_hal.h"
#include "stm32f10x.h"
#include "hal_gpio.h"

void HAL_SPI_Init(SPI_TypeDef *SPIx,const GPIOx_TypeDef *CSPin, uint8_t enableRemap);
void HAL_SPI_SendByte(SPI_TypeDef* SPIx, uint16_t Data);

#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_HAL_SPI_H_ */
