/*
 * hal_gpio.h
 *
 *  Created on: 2015�~6��2��
 *      Author: Thomas
 */

#ifndef HARDWAREABSTRACTIONLAYER_INC_HAL_GPIO_H_
#define HARDWAREABSTRACTIONLAYER_INC_HAL_GPIO_H_
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "hal_nvic.h"
/* Private typedef -----------------------------------------------------------*/
typedef struct{
	uint16_t gpioPin;
	GPIO_TypeDef *gpioPort;
	uint8_t	gpioPortSource;
	uint8_t gpioPinSource;
	uint8_t adcChannel;
}GPIOx_TypeDef;
/* Private define ------------------------------------------------------------*/
#define GPIO_SPEED_SETTING	GPIO_Speed_10MHz
#define NO_ADC_CHANNEL	0xff
/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
extern const GPIOx_TypeDef *PA0;
extern const GPIOx_TypeDef *PA1;
extern const GPIOx_TypeDef *PA2;
extern const GPIOx_TypeDef *PA3;
extern const GPIOx_TypeDef *PA4;
extern const GPIOx_TypeDef *PA5;
extern const GPIOx_TypeDef *PA6;
extern const GPIOx_TypeDef *PA7;
extern const GPIOx_TypeDef *PA8;
extern const GPIOx_TypeDef *PA9;
extern const GPIOx_TypeDef *PA10;
extern const GPIOx_TypeDef *PA11;
extern const GPIOx_TypeDef *PA12;
extern const GPIOx_TypeDef *PA13;
extern const GPIOx_TypeDef *PA14;
extern const GPIOx_TypeDef *PA15;

extern const GPIOx_TypeDef *PB0;
extern const GPIOx_TypeDef *PB1;
extern const GPIOx_TypeDef *PB2;
extern const GPIOx_TypeDef *PB3;
extern const GPIOx_TypeDef *PB4;
extern const GPIOx_TypeDef *PB5;
extern const GPIOx_TypeDef *PB6;
extern const GPIOx_TypeDef *PB7;
extern const GPIOx_TypeDef *PB8;
extern const GPIOx_TypeDef *PB9;
extern const GPIOx_TypeDef *PB10;
extern const GPIOx_TypeDef *PB11;
extern const GPIOx_TypeDef *PB12;
extern const GPIOx_TypeDef *PB13;
extern const GPIOx_TypeDef *PB14;
extern const GPIOx_TypeDef *PB15;

extern const GPIOx_TypeDef *PC0;
extern const GPIOx_TypeDef *PC1;
extern const GPIOx_TypeDef *PC2;
extern const GPIOx_TypeDef *PC3;
extern const GPIOx_TypeDef *PC4;
extern const GPIOx_TypeDef *PC5;
extern const GPIOx_TypeDef *PC6;
extern const GPIOx_TypeDef *PC7;
extern const GPIOx_TypeDef *PC8;
extern const GPIOx_TypeDef *PC9;
extern const GPIOx_TypeDef *PC10;
extern const GPIOx_TypeDef *PC11;
extern const GPIOx_TypeDef *PC12;
extern const GPIOx_TypeDef *PC13;
extern const GPIOx_TypeDef *PC14;
extern const GPIOx_TypeDef *PC15;

extern const GPIOx_TypeDef *PD0;
extern const GPIOx_TypeDef *PD1;
extern const GPIOx_TypeDef *PD2;
extern const GPIOx_TypeDef *PD3;
extern const GPIOx_TypeDef *PD4;
extern const GPIOx_TypeDef *PD5;
extern const GPIOx_TypeDef *PD6;
extern const GPIOx_TypeDef *PD7;
extern const GPIOx_TypeDef *PD8;
extern const GPIOx_TypeDef *PD9;
extern const GPIOx_TypeDef *PD10;
extern const GPIOx_TypeDef *PD11;
extern const GPIOx_TypeDef *PD12;
extern const GPIOx_TypeDef *PD13;
extern const GPIOx_TypeDef *PD14;
extern const GPIOx_TypeDef *PD15;
/* Exported functions ------------------------------------------------------- */
uint8_t HAL_GPIO_Init(const GPIOx_TypeDef  *GPIOx, GPIOMode_TypeDef GPIO_MODE);
uint8_t HAL_GPIO_ReadPin(const GPIOx_TypeDef  *GPIOx);
void HAL_GPIO_SetON(const GPIOx_TypeDef  *GPIOx);
void HAL_GPIO_SetOFF(const GPIOx_TypeDef  *GPIOx);
void HAL_GPIO_SetJTAGDPAsGpio(void);
void HAL_GPIO_EXTI_Init(const GPIOx_TypeDef  *GPIOx,const EXTITrigger_TypeDef Trigger_Mode);
void HAL_GPIO_MCOEnable(void);
void HAL_GPIO_InitGpio_PP(const GPIOx_TypeDef  *GPIOx);
void HAL_GPIO_InitGpio_OD(const GPIOx_TypeDef  *GPIOx);
void HAL_GPIO_InitGpio_Floating(const GPIOx_TypeDef  *GPIOx);

#endif /* HARDWAREABSTRACTIONLAYER_INC_HAL_GPIO_H_ */
