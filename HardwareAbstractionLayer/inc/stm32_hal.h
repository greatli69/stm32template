/*
 * stm32_hal.h
 *
 *  Created on: 2015�~6��8��
 *      Author: Thomas
 */

#ifndef LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_STM32_HAL_H_
#define LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_STM32_HAL_H_

#include "stm32f10x.h"

#include "utility.h"
#include "hal_nvic.h"
#include "hal_gpio.h"
#include "hal_timer.h"
#include "hal_usart.h"
#include "hal_spi.h"
#include "hal_rcc.h"
#include "hal_flash.h"
#include "hal_i2c.h"
#include "hal_adc.h"
#include "hal_dma.h"
#include "hal_backup.h"

#define REMAP_ENABLE ENABLE
#define REMAP_DISABLE DISABLE

#endif /* LIBRARIES_HARDWAREABSTRACTIONLAYER_INC_STM32_HAL_H_ */
